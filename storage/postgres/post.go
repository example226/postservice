package postgres

import (
	"database/sql"
	"fmt"

	pb "gitlab.com/test-kafka/postservice/genproto/customer"
	pbp "gitlab.com/test-kafka/postservice/genproto/post"

	"log"

	"github.com/jmoiron/sqlx"
)

type postRepo struct {
	db *sqlx.DB
}

// NewPostRepo ...

func NewPostRepo(db *sqlx.DB) *postRepo {
	return &postRepo{db: db}
}

func (r *postRepo) CreatePost(post *pbp.PostReq) (*pbp.PostResp, error) {
	postResp := pbp.PostResp{}
	err := r.db.QueryRow(`insert into post (name,description,customer_id) 
	values ($1,$2,$3) 
	returning post_id,
	name,description,customer_id`,
		post.Name, post.Description,
		post.Uuid).Scan(
		&postResp.PostId, &postResp.Name,
		&postResp.Description, &postResp.Uuid)
	if err != nil {
		return &pbp.PostResp{}, err
	}
	med := []*pbp.MediasResp{}
	for _, media := range post.Medias {
		media, err := r.CreatMedia(media)
		if err != nil {
			return nil, err
		}
		media.PostId = postResp.PostId
		med = append(med, media)
	}
	postResp.Medias = med

	return &postResp, nil
}

func (r *postRepo) CreateCustomer(req *pb.CustomerResp) error {
	// var err error
	fmt.Print("Bu create methodda  ", req)
	_, err := r.db.Exec(`INSERT INTO post_users (first_name,
		last_name,bio,email,password,username,phonenumber)values($1,$2,
		$3,$4,$5,$6,$7)`, req.FirstName,
		req.LastName, req.Bio, req.Email, req.Password,
		req.Username, req.PhoneNumber)
	if err != nil {
		return err
	}

	for _, i := range req.Addresses {
		_, err := r.db.Exec(`INSERT INTO addresses(
			district,street)values($1,$2)`,
			i.District, i.Street)
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *postRepo) GetPostById(ids *pbp.ID) (*pbp.Post, error) {
	tempPost := &pbp.Post{}
	err := r.db.QueryRow(`select name,
	description,
	customer_id,
	post_id from post 
	where post_id=$1 and deleted_at is null`,
		ids.Id).Scan(
		&tempPost.Name,
		&tempPost.Description, &tempPost.Uuid, &tempPost.PostId)
	if err == sql.ErrNoRows {
		return &pbp.Post{}, nil
	}
	if err != nil {
		log.Fatal("Error while select owners", err)
		return &pbp.Post{}, err
	}
	tempPost.Medias, err = r.GetMediasByPostId(&pbp.ID{Id: tempPost.PostId})
	fmt.Println(tempPost)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return &pbp.Post{}, err
	}
	return tempPost, nil
}

func (r *postRepo) UpdatePost(post *pbp.Post) (*pbp.Post, error) {
	postResp := &pbp.Post{}
	if post.Uuid != "" {
		fmt.Println("Bu uuid bilan", postResp)
		err := r.db.QueryRow(`UPDATE post SET updated_at=NOW(),name=$1,
	description=$2 where 
	post_id=$3 and customer_id=$4 and deleted_at is null 
	returning post_id,name,description,customer_id`,
			post.Name, post.Description,
			post.PostId, post.Uuid).Scan(
			&postResp.PostId,
			&postResp.Name, &postResp.Description,
			&postResp.Uuid)
		if err == sql.ErrNoRows {
			return nil, nil
		}
		if err != nil {
			log.Fatal("Error while update post", err)
			return &pbp.Post{}, err
		}
	} else {
		fmt.Println("Bu o'zi", postResp)
		err := r.db.QueryRow(`UPDATE post SET updated_at=NOW(),name=$1,
	description=$2 where 
	post_id=$3 and deleted_at is null 
	returning post_id,name,description,customer_id`,
			post.Name, post.Description,
			post.PostId).Scan(
			&postResp.PostId,
			&postResp.Name, &postResp.Description,
			&postResp.Uuid)
		if err == sql.ErrNoRows {
			return nil, nil
		}
		if err != nil {
			log.Fatal("Error while update post", err)
			return &pbp.Post{}, err
		}
	}
	fmt.Println("Mediaga kelib", postResp)
	for _, media := range post.Medias {
		medias := pbp.MediasResp{}
		err := r.db.QueryRow(`
		UPDATE medias SET updated_at=NOW(),name=$1,link=$2,
		type=$3 where post_id=$4 and deleted_at is null 
		returning id,name,link,type`, media.Name, media.Link, media.Type, post.PostId).Scan(
			&medias.Id, &medias.Name, &medias.Link, &medias.Type)
		if err != nil {
			return &pbp.Post{}, err
		}
		postResp.Medias = append(postResp.Medias, &medias)
	}
	fmt.Println("bu oxirida", postResp)
	return postResp, nil

}

func (r *postRepo) GetByCustId(req *pbp.Ids) ([]*pbp.Post, error) {
	fmt.Println("UUid", req.Uuid)
	rows, err := r.db.Query(`SELECT
		name,
		description,
		customer_id,
		post_id
		FROM post
		where customer_id=$1 and deleted_at is null`, req.Uuid)
	if err == sql.ErrNoRows {
		return []*pbp.Post{}, nil
	}
	fmt.Println("Bu err dan oldin", err)
	if err != nil {
		return []*pbp.Post{}, err
	}
	defer rows.Close()

	response := []*pbp.Post{}
	fmt.Println("Bu for dan oldin")
	for rows.Next() {
		temp := &pbp.Post{}
		err = rows.Scan(
			&temp.Name,
			&temp.Description,
			&temp.Uuid,
			&temp.PostId,
		)
		if err != nil {
			return []*pbp.Post{}, err
		}
		temp.Medias, err = r.GetMediasByPostId(&pbp.ID{Id: temp.PostId})
		fmt.Println(temp)
		if err != nil {
			temp.Medias = nil
		}
		response = append(response, temp)
	}
	return response, nil
}

func (r *postRepo) DeleteDatabase(ids *pbp.ID) (*pbp.Empty, error) {
	_, err := r.db.Exec(`UPDATE post SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, ids.Id)
	if err == sql.ErrNoRows {
		return &pbp.Empty{}, nil
	}
	if err != nil {
		log.Fatal("Error while delete post by post id", err)
		return &pbp.Empty{}, err
	}

	return &pbp.Empty{}, nil
}

func (r *postRepo) DeleteByCustID(req *pbp.Ids) error {
	fmt.Println(req)
	_, err := r.db.Exec(`
		update post SET deleted_at=NOW() where customer_id=$1 and deleted_at is null
	`, req.Uuid)
	if err == sql.ErrNoRows {
		return nil
	}
	if err != nil {
		log.Fatal("Error while delete post by customer id", err)
		return err
	}
	return nil
}

func (r *postRepo) GetListPostBySearch(req *pbp.GetListBySearch) ([]*pbp.Post, error) {
	offset := (req.Page - 1) * req.Limit
	search := fmt.Sprintf("%s LIKE $1", req.Search.Field)
	order := fmt.Sprintf("ORDER BY %s %s", req.Orders.Field, req.Orders.Values)

	query := `SELECT 
	name,
	customer_id,
	description 
	FROM post
	where deleted_at is null` + " and " + search + " " + order + " "

	rows, err := r.db.Query(query+"LIMIT $2 OFFSET $3", "%"+req.Search.Values+"%", req.Limit, offset)
	if err == sql.ErrNoRows {
		return []*pbp.Post{}, nil
	}
	if err != nil {
		return []*pbp.Post{}, err
	}
	defer rows.Close()
	response := []*pbp.Post{}

	for rows.Next() {
		temp := &pbp.Post{}
		err = rows.Scan(
			&temp.Name,
			&temp.Description,
			&temp.Uuid,
		)
		if err != nil {
			return response, err
		}
		response = append(response, temp)
	}
	return response, nil
}
