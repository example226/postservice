package repo

import (
	pb "gitlab.com/test-kafka/postservice/genproto/customer"
	pbp "gitlab.com/test-kafka/postservice/genproto/post"
)

// PostStorageI ...
type PostStorageI interface {
	CreatePost(*pbp.PostReq) (*pbp.PostResp, error)
	CreateCustomer(*pb.CustomerResp) error
	GetPostById(*pbp.ID) (*pbp.Post, error)
	GetByCustId(*pbp.Ids) ([]*pbp.Post, error)
	UpdatePost(*pbp.Post) (*pbp.Post, error)
	DeleteDatabase(*pbp.ID) (*pbp.Empty, error)
	DeleteByCustID(*pbp.Ids) error
	GetListPostBySearch(*pbp.GetListBySearch) ([]*pbp.Post, error)
}
