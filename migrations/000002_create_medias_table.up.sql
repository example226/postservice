CREATE TABLE IF NOT EXISTS medias(
    post_id int REFERENCES post(post_id),
    name TEXT NOT NULL,
    link TEXT NOT NULL,
    type TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP);