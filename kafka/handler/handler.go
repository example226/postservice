package kafka

import (
	"gitlab.com/test-kafka/postservice/config"
	pb "gitlab.com/test-kafka/postservice/genproto/customer"
	"gitlab.com/test-kafka/postservice/pkg/logger"
	"gitlab.com/test-kafka/postservice/storage"
)

type KafkaHandler struct {
	config  config.Config
	storage storage.IStorage
	log     logger.Logger
}

func NewKafkaHandlerFunc(config config.Config, storage storage.IStorage, log logger.Logger) *KafkaHandler {
	return &KafkaHandler{
		storage: storage,
		config:  config,
		log:     log,
	}
}

func (h *KafkaHandler) Handle(value []byte) error {
	user := pb.CustomerResp{}
	err := user.Unmarshal(value)
	if err != nil {
		return err
	}
	err = h.storage.Post().CreateCustomer(&user)
	if err != nil {
		return err
	}
	return nil
}
