package grpcClient

import (
	"fmt"

	"gitlab.com/test-kafka/postservice/config"
	pb "gitlab.com/test-kafka/postservice/genproto/customer"
	pbr "gitlab.com/test-kafka/postservice/genproto/review"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManager struct {
	conf            config.Config
	reviewservice   pbr.ReviewServiceClient
	customerservice pb.CustomerServiceClient
}

func New(cnfg config.Config) (*ServiceManager, error) {
	connReview, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cnfg.ReviewServiceHost, cnfg.ReviewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial reviews service: host: %s and port: %d",
			cnfg.ReviewServiceHost, cnfg.ReviewServicePort)

	}

	connCustomer, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cnfg.CustomerServiceHost, cnfg.CustomerServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial customer service: host: %s and port: %d",
			cnfg.ReviewServiceHost, cnfg.ReviewServicePort)

	}

	serviceManager := &ServiceManager{
		conf:            cnfg,
		reviewservice:   pbr.NewReviewServiceClient(connReview),
		customerservice: pb.NewCustomerServiceClient(connCustomer),
	}

	return serviceManager, nil
}

func (s *ServiceManager) ReviewService() pbr.ReviewServiceClient {
	return s.reviewservice
}

func (s *ServiceManager) CustomerService() pb.CustomerServiceClient {
	return s.customerservice
}
