package service

import (
	"context"

	pb "gitlab.com/test-kafka/postservice/genproto/customer"
	l "gitlab.com/test-kafka/postservice/pkg/logger"
	grpcclient "gitlab.com/test-kafka/postservice/service/grpc_client"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CustomerService ...
type CustomerService struct {
	customer *grpcclient.ServiceManager
	logger   l.Logger
}

func (s *CustomerService) CreateCustomer(ctx context.Context, req *pb.CustomerReq) (*pb.CustomerResp, error) {
	user, err := s.customer.CustomerService().CreateCustomer(ctx, req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert customer", err))
		return &pb.CustomerResp{}, status.Error(codes.Internal, "something went wrong, please check customer info")
	}
	return user, nil
}

func (s *CustomerService) GetCustomerById(ctx context.Context, req *pb.ID) (*pb.CustomerResp, error) {
	_, err := s.customer.CustomerService().GetCustomerById(ctx, req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error select users", err))
		return &pb.CustomerResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return &pb.CustomerResp{}, nil
}

func (s *CustomerService) UpdateCustomer(ctx context.Context, req *pb.Customer) (*pb.Customer, error) {
	res, err := s.customer.CustomerService().UpdateCustomer(ctx, req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Update", err))
		return &pb.Customer{}, status.Error(codes.InvalidArgument, "Please check customer info")
	}
	return res, nil

}

func (s *CustomerService) DeleteCustomer(ctx context.Context, req *pb.ID) (*pb.Empty, error) {
	_, err := s.customer.CustomerService().DeleteCustomer(ctx, req)
	if err != nil {
		s.logger.Error("Error while delete customer", l.Any("Delete", err))
		return &pb.Empty{}, status.Error(codes.InvalidArgument, "wrong id for delete")
	}
	return &pb.Empty{}, nil
}

func (s *CustomerService) GetCustomers(ctx context.Context, req *pb.ID) (*pb.GetCustomer, error) {
	customer, err := s.customer.CustomerService().GetCustomerAllinfo(ctx, req)
	if err != nil {
		s.logger.Error("error while geting customers", l.Any("error getting customers", err))
		return &pb.GetCustomer{}, status.Error(codes.Internal, "something went wrong")
	}
	return customer, nil
}
