package main

import (
	"net"

	"gitlab.com/test-kafka/postservice/config"
	pbp "gitlab.com/test-kafka/postservice/genproto/post"
	"gitlab.com/test-kafka/postservice/kafka"
	"gitlab.com/test-kafka/postservice/pkg/db"
	"gitlab.com/test-kafka/postservice/pkg/logger"
	"gitlab.com/test-kafka/postservice/service"
	grpcclient "gitlab.com/test-kafka/postservice/service/grpc_client"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "template-service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectTODB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("error while connect to clients review", logger.Error(err))
	}

	CustomerCreateTopic := kafka.NewKafkaConsumer(connDB, &cfg, log, "customer.customer")
	go CustomerCreateTopic.Start()

	postService := service.NewPostService(grpcClient, connDB, log)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pbp.RegisterPostServiceServer(s, postService)
	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
