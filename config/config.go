package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment         string //develop, staging, production
	PostgresHost        string
	PostgresPort        int
	PostgresDatabase    string
	PostgresUser        string
	PostgresPassword    string
	ReviewServiceHost   string
	ReviewServicePort   int
	CustomerServiceHost string
	CustomerServicePort int
	// KafkaHost           string
	// KafkaPort           int
	LogLevel            string
	RPCPort             string
}

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT",
		"develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST",
		"database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT",
		5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "postdb1"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "azamali"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "azamali"))

	c.ReviewServiceHost = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReviewServicePort = cast.ToInt(getOrReturnDefault("REVIEW_SERVICE_PORT", 9003))

	c.CustomerServiceHost = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_HOST", "database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com"))
	c.CustomerServicePort = cast.ToInt(getOrReturnDefault("CUSTOMER_SERVICE_PORT", 9001))

	// c.KafkaHost = cast.ToString(getOrReturnDefault("Kafka_HOST", "kafka"))
	// c.KafkaPort = cast.ToInt(getOrReturnDefault("Kafka_PORT", 9092))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.RPCPort = cast.ToString(getOrReturnDefault("RPC_PORT", ":9002"))
	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
